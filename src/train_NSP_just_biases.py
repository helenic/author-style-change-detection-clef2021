import pandas as pd
import numpy as np
from tqdm import tqdm
import torch
from torch import nn
from transformers import BertTokenizer, BertForNextSentencePrediction
from sklearn.metrics import accuracy_score, f1_score
import os
import time
import json
from timeit import default_timer as timer
from utils import *

if __name__ == "__main__":

    # change to 0 or 1, depending on which GPU you want to use
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"

    if torch.cuda.is_available():
        device = torch.device("cuda:0")
        print("Running on the GPU")
    else:
        device = torch.device("cpu")
        print("Running on the CPU")
    seeds = [3213123]
    iter_last = []
    for i, seed in enumerate(seeds):

        torch.manual_seed(seed=seed)

        hyperparams = {
            "batch_size": 6,
            "hidden_sizes": [1024, 256],
            "dropout": 0.5,
            "n_epochs": 3,
            "lr": 1e-5,
            "use_schedulers": True,
            "scheduler_gamma": 0.9,
            "print_every_n_batches": 1000,
            "early_stopping_patience": 5,
            "seed": seed,
        }

        # MODELS_DIR = "models/massive_experiment_mali_lr1e-5_cleaned_scheduler"
        MODELS_DIR = f"models/test_time_performance_head"
        if not os.path.exists(MODELS_DIR):
            os.makedirs(MODELS_DIR)
        with open(os.path.join(MODELS_DIR, "logs.txt"), "a") as f:
            f.write(str(hyperparams) + "\n")

        BEST_MODEL_PATH = os.path.join(MODELS_DIR, "NSP_best_model_on_validation.pt")
        DATASET_PATH = "datasets/non_massive_train.csv"

        tokenizer = BertTokenizer.from_pretrained("bert-base-cased")
        bert = BertForNextSentencePrediction.from_pretrained("bert-base-cased")
        bert = bert.to(device)

        bce_logits_loss = nn.BCEWithLogitsLoss()

        train_df = pd.read_csv(DATASET_PATH)
        print(
            "Train dataframe value counts:\n", train_df["author_change"].value_counts()
        )

        valid_df = pd.read_csv("datasets/valid.csv")
        print(
            "Valid dataframe value counts:\n", valid_df["author_change"].value_counts()
        )

        train_data_loader = torch.utils.data.DataLoader(
            CustomDataset(train_df), shuffle=False, batch_size=hyperparams["batch_size"]
        )
        valid_data_loader = torch.utils.data.DataLoader(
            CustomDataset(valid_df), shuffle=False, batch_size=hyperparams["batch_size"]
        )
        optim_parameters = []
        learning_params = 0

        for n, p in bert.named_parameters():
            if "cls" in n:
                print(n)
                optim_parameters.append(p)
                nn = 1
                for s in list(p.size()):
                    nn = nn * s
                learning_params += nn

        with open(os.path.join(MODELS_DIR, "logs.txt"), "a") as f:
            f.write(f"\nLEARNING PARAMS: {learning_params}\n")

        optim = torch.optim.AdamW(optim_parameters, lr=hyperparams["lr"])

        if hyperparams["use_schedulers"]:
            scheduler = torch.optim.lr_scheduler.ExponentialLR(
                optim, gamma=hyperparams["scheduler_gamma"]
            )

        train_losses_per_epoch = []
        valid_losses_per_epoch = []
        output_dictionaries = []

        t0 = time.time()
        best_average_epochs_loss_valid = 10000  # used for early stopping
        trigger_counter = 0

        for epoch in range(hyperparams["n_epochs"]):
            t0_epoch = time.time()
            epoch_loss_train = 0

            # ---------
            # TRAINING
            # ---------

            print("=" * 10 + f" EPOCH {epoch+1} " + "=" * 10)
            print("=" * 10 + " TRAINING " + "=" * 10)

            bert.train()
            for i, (first_texts, second_texts, author_changes) in tqdm(
                enumerate(train_data_loader), total=len(train_data_loader)
            ):

                if i % hyperparams["print_every_n_batches"] == 0 and i != 0:
                    print(
                        f"Batch {(i+1):>5} of {len(train_data_loader):>5}. Elapsed: {format_seconds(time.time() - t0_epoch)}"
                    )

                author_changes = author_changes.to(device)

                encoding = tokenizer(
                    first_texts,
                    second_texts,
                    padding="max_length",
                    truncation="longest_first",
                    return_tensors="pt",
                )
                encoding = encoding.to(device)
                t_s = timer()
                outputs = bert(**encoding, labels=author_changes)
                loss = outputs.loss
                optim.zero_grad()
                loss.backward()
                optim.step()
                t_e = timer()
                iter_last.append(t_e - t_s)

                if i == 200:
                    with open(os.path.join(MODELS_DIR, "logs.txt"), "a") as f:
                        f.write(str(np.mean(np.array(iter_last))))
                        print(str(np.mean(np.array(iter_last))))

                epoch_loss_train += loss.item()

            if hyperparams["use_schedulers"]:
                scheduler.step()

            average_epoch_loss_train = epoch_loss_train / len(train_data_loader)
            train_losses_per_epoch.append(average_epoch_loss_train)
            epoch_train_duration = time.time() - t0_epoch

            print("\n")
            print(f"Average loss on train: {average_epoch_loss_train:.8f}")
            print(f"Epoch training time: {format_seconds(epoch_train_duration)}")
            print("\n")

            # -----------
            # EVALUATION
            # -----------

            bert.eval()

            t0_valid = time.time()
            print("=" * 10 + " VALIDATING " + "=" * 10)
            y_true = []
            y_pred = []
            epoch_loss_valid = 0

            with torch.no_grad():
                for i, (first_texts, second_texts, author_changes) in enumerate(
                    tqdm(valid_data_loader, total=len(valid_data_loader))
                ):
                    author_changes = author_changes.to(device)

                    encoding = tokenizer(
                        first_texts,
                        second_texts,
                        padding="max_length",
                        truncation="longest_first",
                        return_tensors="pt",
                    )
                    encoding = encoding.to(device)
                    outputs = bert(**encoding, labels=author_changes)
                    loss = outputs.loss
                    logits = outputs.logits

                    epoch_loss_valid += loss.item()

                    y_pred.extend(torch.argmax(logits, dim=1).detach().cpu())
                    y_true.extend(
                        torch.reshape(author_changes, (-1,)).float().detach().cpu()
                    )

            average_epoch_loss_valid = epoch_loss_valid / len(valid_data_loader)

            if average_epoch_loss_valid > best_average_epochs_loss_valid:
                trigger_counter += 1
            else:
                trigger_counter = 0
                best_average_epochs_loss_valid = average_epoch_loss_valid
                torch.save(
                    {
                        "epoch": epoch,
                        "bert_state_dict": bert.state_dict(),
                        "optimizer_state_dict": optim.state_dict(),
                        "average_epoch_loss_valid": average_epoch_loss_valid,
                    },
                    BEST_MODEL_PATH,
                )

                # load the models with:
                # checkpoint = torch.load(PATH)
                # bert.load_state_dict(checkpoint['bert_state_dict'])
                # discriminator.load_state_dict(checkpoint['discriminator_state_dict'])
                # bert.eval()
                # discriminator.eval()

            valid_losses_per_epoch.append(average_epoch_loss_valid)

            y_true = torch.stack(y_true).numpy()
            y_pred = torch.stack(y_pred).numpy()

            valid_accuracy = accuracy_score(y_true, y_pred)
            valid_f1_score = f1_score(y_true, y_pred, average="binary")
            epoch_valid_duration = time.time() - t0_valid

            print("\n")
            print(f"Validation accuracy: {valid_accuracy:.8f}")
            print(f"Validation f1-score: {valid_f1_score:.8f}")
            print(f"Average loss on valid: {average_epoch_loss_valid:.8f}")
            print(f"Epoch validating time: {format_seconds(epoch_valid_duration)}")
            print("\n")

            output_dictionaries.append(
                {
                    "epoch": epoch + 1,
                    "validation accuracy": valid_accuracy,
                    "validation f1-score": valid_f1_score,
                    "average loss on train": average_epoch_loss_train,
                    "average loss on valid": average_epoch_loss_valid,
                    "time spent training": format_seconds(epoch_train_duration),
                    "time spent validating": format_seconds(epoch_valid_duration),
                    "early stopping trigger": trigger_counter,
                }
            )

            if trigger_counter >= hyperparams["early_stopping_patience"]:
                print("EARLY STOPPING! Taking the best (saved) model.")
                break

        with open(os.path.join(MODELS_DIR, "logs.txt"), "a") as f:
            f.write(json.dumps(output_dictionaries, sort_keys=False, indent=4))

    print(f"FINISHED! Training took {format_seconds(time.time() - t0)} in total.")
