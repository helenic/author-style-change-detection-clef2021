import pandas as pd
import numpy as np
from tqdm import tqdm
import torch
from transformers import BertModel, BertTokenizer
import en_core_web_sm


nlp = en_core_web_sm.load(enable="sentencizer", disable=["tagger", "ner"])
tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
model = BertModel.from_pretrained("bert-base-cased")
model.eval() # don't train the weights, just use pretrained ones
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model.to(device) # send the whole bert model on cuda
print('Device in use:', device)

def get_paragraph_representation(paragraph, tokenizer, model, nlp):
    
    representations_per_sentence = [] # nr_of_sentences x 768

    for sentence in nlp(paragraph).sents:
        encoded_input = tokenizer(sentence.text, return_tensors='pt').to(device)
        output = None
        with torch.no_grad():
            output = model(**encoded_input, output_hidden_states=True) # model output
        hidden_states = output[2] # take hidden states only; index 0 is input embeddings, indices 1-12 hidden layers
        bert_embeddings = torch.stack(hidden_states, dim=0) # 13x1x6x768 (n_layers x batch_size x sentence_len x embedding_len)
        bert_embeddings = torch.squeeze(bert_embeddings, dim=1) # 13x6x768
        bert_embeddings = bert_embeddings.permute(1, 0, 2) # 6x13x768

        token_vectors_sum = [] # sum of last 4 hidden layers for every token separately
        # sentence_len x 768

        for token in bert_embeddings:
            # token is a 13 x 768 tensor

            # sum the vectors from the last four layers (for each token)
            sum_vec = torch.sum(token[-4:], dim=0)

            token_vectors_sum.append(sum_vec)

        sentence_representation = torch.stack(token_vectors_sum, dim=0).sum(dim=0) # summing representations for each token

        representations_per_sentence.append(sentence_representation)

    paragraph_representation = torch.stack(representations_per_sentence, dim=0).sum(dim=0) # summing representations for each sentence

    return (paragraph_representation, len(representations_per_sentence))


paragraph = "This is a tryout text."
representation, nr_of_sentences = get_paragraph_representation(paragraph, tokenizer, model, nlp)
print(representation.size(), nr_of_sentences)

df = pd.read_csv("datasets/massive.csv")
print(df['author_change'].value_counts())

smaller_df = df.head(50)

column_names = []
for i in range(768):
    column_names.append('w' + str(i))
column_names.append('author_change')

smaller_x_df = pd.DataFrame(columns=column_names)

for index, row in smaller_df.iterrows():
    first_paragraph = row['first_text'].strip()
    second_paragraph = row['second_text'].strip()
    
    representation_1, nr_of_sentences_1 = get_paragraph_representation(first_paragraph, tokenizer, model, nlp)
    representation_2, nr_of_sentences_2 = get_paragraph_representation(second_paragraph, tokenizer, model, nlp)
    
    representation = (representation_1 + representation_2) / (nr_of_sentences_1 + nr_of_sentences_2)
    to_append = representation.detach().to("cpu").numpy().tolist()
    to_append.append(row['author_change'])
            
    smaller_x_df.loc[index] = to_append


print(smaller_x_df.iloc[:10, :])

X_task_2 = pd.DataFrame(columns=column_names)

for index, row in tqdm(df.iterrows()):
    first_paragraph = row['first_text'].strip()
    second_paragraph = row['second_text'].strip()
    
    representation_1, nr_of_sentences_1 = get_paragraph_representation(first_paragraph, tokenizer, model, nlp)
    representation_2, nr_of_sentences_2 = get_paragraph_representation(second_paragraph, tokenizer, model, nlp)
    
    representation = (representation_1 + representation_2) / (nr_of_sentences_1 + nr_of_sentences_2)
    to_append = representation.detach().to("cpu").numpy().tolist()
    to_append.append(row['author_change'])
            
    X_task_2.loc[index] = to_append

X_task_2.to_csv('datasets/train_task_2.csv', index=False)
